---
title: "¿Qué Es Hama?"
date: 2021-03-21T13:28:31+01:00
description: "¿Qué es Hama?"
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: Admin
authorEmoji: 💻
pinned: true
tags:
- hama
series:
-
categories:
- hama
image: images/hama.jpeg
---

Cuentas y placas de clavijas de muchos colores. Colocando las cuentas es posible hacer diseños tanto sencillos como complicados.

Es simplemente cuestión de ir colocando cuentas en la placa de clavijas hasta obtener la imagen que se desee.

https://www.hama.dk/es/

![planchar hamas](/images/planchar-hamas.png)
